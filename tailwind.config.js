module.exports = {
  theme: {
    screens: {
      sm: '640px',
      md: '960px',
      lg: '1280px',
      xl: '1920px',
    },
    colors: {
      white: '#fff',
      gray: {
        300: '#e2e8f0',
        800: '#2d3748',
      },
    },
  },
  variants: {},
  plugins: [],
}
