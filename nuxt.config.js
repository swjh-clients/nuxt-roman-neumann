import path from 'path'
import * as constants from './constants.js'
import axios from 'axios'

module.exports = {
  target: 'static',

  /*
   ** Headers of the page
   */
  head() {
    return {
      htmlAttrs: {
        lang: 'de',
      },
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        {
          rel: 'preconnect',
          href: 'http://img2.storyblok.com/',
          crossorigin: 'anonymous',
        },
        {
          rel: 'preconnect',
          href: 'https://img2.storyblok.com/',
          crossorigin: 'anonymous',
        },
        {
          rel: 'preconnect',
          href: 'https://fonts.gstatic.com/',
          crossorigin: 'anonymous',
        },
        {
          rel: 'preconnect',
          href: 'https://www.google-analytics.com',
          crossorigin: 'anonymous',
        },
        {
          rel: 'alternate',
          href: 'http://www.roman-neumann.com' + '/',
          hreflang: 'de',
        },
        {
          rel: 'alternate',
          href: 'http://www.roman-neumann.com' + '/en',
          hreflang: 'en',
        },
      ],
    }
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: constants.color2 },

  /*
   ** Global CSS
   */
  css: [
    'assets/css/tailwind.css', // includes normalize.css
    'assets/css/main.scss',
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/vue-scrollto.js',
    '~/plugins/vue-truncate-collapsed.js',
    '~/plugins/vue-scroll-reveal.client.js',
    '~/plugins/ga.client.js',
    '~/plugins/v-lazy-image.js',
    '~/plugins/globalComponents.js',
    '~/plugins/filters.js',
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Axios
    // Doc: https://github.com/nuxt-community/axios-module#usage
    ['@nuxtjs/axios'],
    // Storyblok https://github.com/storyblok/storyblok-nuxt
    [
      'storyblok-nuxt',
      {
        accessToken: constants.sbAccessToken,
      },
    ],
    // globally available SCSS: https://github.com/nuxt-community/style-resources-module
    ['@nuxtjs/style-resources'],
    // https://github.com/nuxt-community/sitemap-module
    // sitemap module has to be last module!
    /* [
      '@nuxtjs/sitemap',
      {
        path: '/sitemap.xml',
        hostname: 'https://www.roman-neumann.com',
        exclude: ['/thank-you'],
        // filter ({ routes, options }) {
        //   if (options.hostname === 'example.com') {
        //     return routes.filter(route => route.locale === 'en')
        //   }
        //   return routes.filter(route => route.locale === 'fr')
        // }
      },
    ], */
    '@nuxtjs/tailwindcss',
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    postcss: {
      // https://nuxtjs.org/faq/postcss-plugins/
      preset: {
        // Change the postcss-preset-env settings
        stage: 0,
        autoprefixer: {
          cascade: false, // don't indent prefixes
          // grid: 'autoplace', // enable IE polyfill for css grid
        },
        cssnano: {
          preset: 'default',
          discardComments: { removeAll: true },
          // zindex: false,
        },
      },
    },
    extend(config, ctx) {},
    extractCSS: false,
  },
  /*
   * Render configuration
   */
  render: {
    http2: { push: true },
  },
  /*
   * Generate configuration
   */
  generate: {
    routes: function() {
      return axios
        .get(
          constants.sbGetUrlProd +
          '&filter_query[component][in]=' +
          constants.sbPageComponents + // get all components that should be generated as pages
            '&excluding_ids=' +
            constants.sbIndexPageId // exclude /index page (so it is not generated twice)
        )
        .then(res => {
          const pages = res.data.stories.map(page => page.full_slug)
          return [
            '/', // home page
            ...pages, // pages from storyblok
          ]
        })
    },
  },
  purgeCSS: {
    // your settings here
    mode: 'postcss',
    whitelist: [
      'nuxt-progress',
      'is-active',
      'nuxt-link-exact-active',
      'nuxt-link-active',
    ],
    whitelistPatterns: [/page/],
  },
  styleResources: {
    // global style imports here
    scss: ['~/assets/css/mixins.scss'],
  },
}
