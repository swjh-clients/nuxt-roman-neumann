// Project related
export const baseUrl = 'http://www.roman-neumann.com'
export const baseUrlSecure = 'https://www.roman-neumann.com'
export const twitter_id = null
export const color1 = '#3172ae'
export const color2 = '#47e6eb'

// Storyblok (project-related)
export const sbPublicToken = 'e0k4E37OAO1XkPc9THCWfwtt'
export const sbPreviewToken = 'nh9SYiJmIwPjKZZIqfzQUwtt'
export const sbIndexPageId = '897442' // index/home page id
export const sbPageComponents = 'PageGeneral,PageHome' // IMPORTANT! Add all Components that should be generated as pages (comma-separated)

// Storyblok (general)
export const sbAccessToken =
  process.env.NODE_ENV == 'production' ? sbPublicToken : sbPreviewToken
const sbVersion = process.env.NODE_ENV == 'production' ? 'published' : 'draft'

const sbApiUrl = 'https://api.storyblok.com/v1/cdn/stories'
export const sbGetUrl =
  sbApiUrl +
  '?version=' +
  sbVersion +
  '&token=' +
  sbAccessToken +
  '&cv=' +
  Math.floor(Date.now() / 1e3)
export const sbGetUrlProd =
  sbApiUrl +
  '?version=' +
  'published' +
  '&token=' +
  sbPublicToken +
  '&cv=' +
  Math.floor(Date.now() / 1e3)
