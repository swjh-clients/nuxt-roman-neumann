import Vue from 'vue'
import truncate from 'vue-truncate-collapsed'

// Docs: https://github.com/kavalcante/vue-truncate-collapsed
// Define global component:
Vue.component('truncate', truncate)
