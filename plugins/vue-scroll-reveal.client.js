import Vue from 'vue'
import VueScrollReveal from 'vue-scroll-reveal'

Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal', // A CSS class applied to elements with the v-scroll-reveal directive; useful for animation overrides.
  delay: 250,
  duration: 800,
  easing: 'cubic-bezier(0.2, 0.7, 0.6, 1)',
  origin: 'bottom',
  distance: '10px',
  // scale: 1,
  // mobile: false,
})


/* usage on elements:
  <section v-scroll-reveal.reset="{ origin: 'left'}">
    <h1>Slightly late tada!</h1>
  </section>
*/