# nuxt-roman-neumann

## TODO:

- translate:

  - cookie message
  - imprint link

- news posts?
- Storyblok
  - switch all assets to https (advanced image)
  - Use Vimeo/YT Video & add Poster
  - transfer to roman
- 2. Rechnung
- GA: Opt out button
- OG + twitter card

- Offerings section: bubble am ende der Striche?
- image sizes + Webp (v-lazy-image statt vue-lazyload?)
- noreferrer noopener to \_blank links

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
